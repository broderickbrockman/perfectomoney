module PerfectoMoney::Core
	module Api
		class ParseTable < ::FaradayMiddleware::ResponseMiddleware
			dependency do
				require 'nokogiri' unless defined? ::Nokogiri
			end
			define_parser do |body|
				doc = Nokogiri::HTML.parse(body)
				arr=[]
				doc.search('//tr').each do |line|
					td = line.search('td')
					arr << [td[0], td[1]]
				end

				captions = arr.shift.map do |item|
					item.text.gsub(/[^A-z]/, '').underscore
				end

				arr.map do |item|
					hash = {}
					captions.each.each_with_index { |value, i| hash[value.to_sym] = item[i].text }
					hash
				end
			end
		end
	end
end
