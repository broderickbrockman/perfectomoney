module PerfectoMoney::Core
	module Api
		Faraday::Response.register_middleware pm_logger: Logger
		Faraday::Response.register_middleware parse_table: ParseTable
		class Base
			URL_PREFIX = 'https://perfectmoney.is'

			extend Forwardable
			include Virtus.model

			attr_accessor :response

			def save
				@response = connection.post(self.class.method_path, params)
				true
			end

			private

			def connection
				@connection ||= ::Faraday.new(url: URL_PREFIX) do |faraday|
					faraday.request :url_encoded # form-encode POST params

					faraday.response :pm_logger
					faraday.response :parse_table

					faraday.adapter Faraday.default_adapter # make requests with Net::HTTP
				end
			end
		end
	end
end