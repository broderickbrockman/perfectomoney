module PerfectoMoney::Core
	module Api
		class Logger < ::Faraday::Response::Middleware
			def initialize(app)
				super(app)
				@logger = ::Logger.new(STDOUT)
			end

			def call(env)
				@logger.debug "Method: #{env[:method].to_s.upcase} URL: #{env[:url].to_s}"
				super
			end

			def on_complete(env)
				@logger.debug "Status: #{env[:status].to_s}"
			end
		end
	end
end