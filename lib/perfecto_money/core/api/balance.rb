module PerfectoMoney::Core
	module Api
		class Balance < Base
			attr_accessor :login
			attr_accessor :password

			def params
				{
						AccountID: login,
						PassPhrase: password
				}
			end

			def self.method_path
				'acct/balance.asp'
			end
		end
	end
end