module PerfectoMoney::Core
	module Api
		class Withdrawal < Base
			attr_accessor :login
			attr_accessor :password
			attr_accessor :payer
			attr_accessor :payee
			attr_accessor :amount
			attr_accessor :payment_id
			attr_accessor :memo

			def params
				{
						AccountID: login,
						PassPhrase: password,
						Payer_Account: payer,
						Payee_Account: payee,
						Amount: amount,
						PAYMENT_ID: payment_id,
						Memo: memo
				}
			end

			def self.method_path
				'acct/confirm.asp'
			end
		end
	end
end