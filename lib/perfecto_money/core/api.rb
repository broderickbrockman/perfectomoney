module PerfectoMoney
	module Core
		module Api
		end
	end
end

require 'perfecto_money/core/api/logger'
require 'perfecto_money/core/api/parse_table'
require 'perfecto_money/core/api/base'
require 'perfecto_money/core/api/balance'
require 'perfecto_money/core/api/withdrawal'