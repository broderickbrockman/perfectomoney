# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'perfecto_money/version'

Gem::Specification.new do |spec|
	spec.name = "perfecto_money"
	spec.version = PerfectoMoney::VERSION
	spec.authors = ["SherwoodColvin"]
	spec.email = ["sherwoodcolvin@gmail.com"]
	spec.summary = %q{It is Rails PerfectMoney SDK}
	spec.description = %q{It is Rails PerfectMoney SDK. Nothing else.}
	spec.homepage = ""
	spec.license = "MIT"

	spec.files = `git ls-files -z`.split("\x0")
	spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
	spec.test_files = spec.files.grep(%r{^(test|spec|features)/})
	spec.require_paths = ["lib"]

	spec.add_development_dependency "bundler", "~> 1.6"
	spec.add_development_dependency "rake"
	spec.add_runtime_dependency "faraday"
	spec.add_runtime_dependency "faraday_middleware"
	spec.add_runtime_dependency "nokogiri"
	spec.add_runtime_dependency "hashie"
end
